def find_occurrences(rp, cp, motif, r, c, mosaic):
    occurrences = []
    count = 0
    for i in range(r - rp + 1):
        for j in range(c - cp + 1):
            match = True
            for x in range(rp):
                for y in range(cp):
                    if motif[x][y] != 0 and motif[x][y] != mosaic[i + x][j + y]:
                        match = False
                        break
                    if not match:
                    break
            if match:
                count += 1
                occurrences.append((i + 1, j + 1))
    return count, occurrences
rp = 2
cp = 2
r = 3
c = 4
motif = ([1,0],[0,1])
mosaic = ([1,2,1,2],[2,1,1,1],[2,2,1,3])
count, occurrences = find_occurrences(rp, cp, motif, r, c, mosaic)
print("Number of occurrences:", count)
print("Occurrences:", occurrences)
                
